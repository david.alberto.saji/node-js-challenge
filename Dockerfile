FROM node:14.15.4-stretch

ENV workdir /var/prod

COPY . ${workdir}/
WORKDIR ${workdir}
RUN npm install

CMD ["npm", "start"]