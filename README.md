# PINFLAG NODE JS CHALLENGE

### Create .env file with the following credentials:
```
DATABASE_URI=postgres://postgres:docker@db:5432/pinflag_challenge
DATABASE_USERNAME=postgres
DATABASE_PASSWORD=docker
DATABASE_NAME=pinflag_challenge
DATABASE_NAME_TEST=database_test
DATABASE_HOST=db
DATABASE_PORT=5432
```

### Launch api
```
docker-compose up --build -d
```

### Run migrations
```
docker-compose exec web npx sequelize-cli db:migrate
```

### Test api
```
docker-compose exec web npm test
```

### API docs

http://localhost:5000/api-docs
