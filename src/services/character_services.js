import axios from 'axios'
import { extractCharacterData } from '../helpers/character_helpers'

const api = axios.create({
  baseURL: 'https://rickandmortyapi.com/api/character'
})

export default class CharacterService {
  async * fetchCharacters (filters) {
    let nextUrl = '/'
    while (nextUrl) {
      const { data: { info, results } } = await api.get(nextUrl, { params: filters })
      nextUrl = info.next
      yield * results.map(extractCharacterData)
    }
  }

  async findMany (limit) {
    const results = []
    if (limit <= 0) return results
    for await (const character of this.fetchCharacters()) {
      if (results.length >= limit) break
      results.push(character)
    }
    return results
  }

  async findByName (name) {
    for await (const character of this.fetchCharacters({ name })) {
      if (character.name === name) return character
    }
    return null
  }
}
