import CharacterService from './character_services'

const fakeData = {
  info: {
    count: 5,
    pages: 2,
    next: 'https://rickandmortyapi.com/api/character/?page=2',
    prev: null
  },
  results: [
    {
      id: 1,
      name: 'Rick Sanchez',
      status: 'Alive',
      species: 'Human',
      type: '',
      gender: 'Male',
      origin: {
        name: 'Earth (C-137)',
        url: 'https://rickandmortyapi.com/api/location/1'
      },
      location: {
        name: 'Citadel of Ricks',
        url: 'https://rickandmortyapi.com/api/location/3'
      },
      image: 'https://rickandmortyapi.com/api/character/avatar/1.jpeg',
      episode: [
        'https://rickandmortyapi.com/api/episode/1',
        'https://rickandmortyapi.com/api/episode/2'
      ],
      url: 'https://rickandmortyapi.com/api/character/1',
      created: '2017-11-04T18:48:46.250Z'
    },
    {
      id: 2,
      name: 'Morty Smith',
      status: 'Alive',
      species: 'Human',
      type: '',
      gender: 'Male',
      origin: {
        name: 'unknown',
        url: ''
      },
      location: {
        name: 'Citadel of Ricks',
        url: 'https://rickandmortyapi.com/api/location/3'
      },
      image: 'https://rickandmortyapi.com/api/character/avatar/2.jpeg',
      episode: [
        'https://rickandmortyapi.com/api/episode/1',
        'https://rickandmortyapi.com/api/episode/2'
      ],
      url: 'https://rickandmortyapi.com/api/character/2',
      created: '2017-11-04T18:50:21.651Z'
    },
    {
      id: 3,
      name: 'Summer Smith',
      status: 'Alive',
      species: 'Human',
      type: '',
      gender: 'Female',
      origin: {
        name: 'Earth (Replacement Dimension)',
        url: 'https://rickandmortyapi.com/api/location/20'
      },
      location: {
        name: 'Earth (Replacement Dimension)',
        url: 'https://rickandmortyapi.com/api/location/20'
      },
      image: 'https://rickandmortyapi.com/api/character/avatar/3.jpeg',
      episode: [
        'https://rickandmortyapi.com/api/episode/6',
        'https://rickandmortyapi.com/api/episode/7'
      ],
      url: 'https://rickandmortyapi.com/api/character/3',
      created: '2017-11-04T19:09:56.428Z'
    }
  ]
}

jest.mock('axios', () => ({
  create: jest.fn().mockReturnValue({
    get: jest.fn().mockResolvedValue({ data: fakeData })
  })
}))

describe('get characters with async generator', () => {
  test('get 2 characters', async () => {
    const gen = await new CharacterService().fetchCharacters()
    expect((await gen.next()).value).toEqual({
      name: 'Rick Sanchez',
      status: 'Alive',
      species: 'Human',
      origin: 'Earth (C-137)'
    })
    expect((await gen.next()).value).toEqual({
      name: 'Morty Smith',
      status: 'Alive',
      species: 'Human',
      origin: 'unknown'
    })
  })
})

describe('get characters with CharacterService', () => {
  test('get 2 characters', async () => {
    const result = await new CharacterService().findMany(2)
    expect(result).toEqual([
      {
        name: 'Rick Sanchez',
        status: 'Alive',
        species: 'Human',
        origin: 'Earth (C-137)'
      },
      {
        name: 'Morty Smith',
        status: 'Alive',
        species: 'Human',
        origin: 'unknown'
      }
    ])
  })
})

describe('find character by name with CharacterService', () => {
  test('find character "Rick Sanchez"', async () => {
    const result = await new CharacterService().findByName('Rick Sanchez')
    expect(result).toEqual({
      name: 'Rick Sanchez',
      status: 'Alive',
      species: 'Human',
      origin: 'Earth (C-137)'
    })
  })
})
