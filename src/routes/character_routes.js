import app from 'express'
import asyncHandler from 'express-async-handler'
import { body, param, query } from 'express-validator'

import { validateRequest } from '../middleware'
import CharacterController from '../controllers/character_controller'
import CharacterService from '../services/character_services'

const routes = app.Router()
const characterService = new CharacterService()
const characterController = new CharacterController(characterService)

/**
 * Character schema
 * @typedef {object} Character
 * @property {string} name.required - Nombre
 * @property {string} status.required - Estatus
 * @property {string} species.required - Especie
 * @property {string} origin.required - Origen
 */

/**
 * GET /character
 * @summary Get at most N characters
 * @tags Character
 * @param {number} limit.query - Limit the number of characters returned (default 20)
 * @return {array<Character>} 200 - Success response
 * @example response - 200 - Example success response
 * [
 *   {
 *     "name": "Rick Sanchez",
 *     "status": "Alive",
 *     "species": "Human",
 *     "origin": "Earth (C-137)"
 *   },
 *   {
 *     "name": "Morty Smith",
 *     "status": "Alive",
 *     "species": "Human",
 *     "origin": "unknown"
 *   }
 * ]
 */
routes.get('/',
  query('limit').default(20).isNumeric({ no_symbols: true }),
  validateRequest,
  asyncHandler(characterController.index)
)

/**
 * POST /character
 * @summary Create a character
 * @tags Character
 * @param {Character} request.body.required - Character info
 * @return {object} 201 - Success response
 * @return {object} 400 - Bad request response
 * @example request - Example character
 * {
 *   "name": "Rick Sanchez",
 *   "status": "Alive",
 *   "species": "Human",
 *   "origin": "Earth (C-137)"
 * }
 * @example response - 201 - Example success response
 * {
 *   "id": 1,
 *   "name": "Rick Sanchez",
 *   "status": "Alive",
 *   "species": "Human",
 *   "origin": "Earth (C-137)"
 * }
 * @example response - 400 - Example error response
 * {
 *   "errors": [
 *     {
 *       "msg": "Invalid value",
 *       "param": "status",
 *       "location": "body"
 *     }
 *   ]
 * }
 */
routes.post('/',
  body(['name', 'status', 'species', 'origin']).notEmpty(),
  validateRequest,
  asyncHandler(characterController.create)
)

/**
 * GET /character/{name}
 * @summary Get character by name
 * @tags Character
 * @param {string} name.path.required - Character name
 * @return {Character} 200 - Success response
 * @return {object} 404 - Not found response
 * @example response - 200 - Example success response
 * {
 *   "name": "Rick Sanchez",
 *   "status": "Alive",
 *   "species": "Human",
 *   "origin": "Earth (C-137)"
 * }
 * @example response - 404 - Example error response
 * {
 *   "error": "There is nothing here"
 * }
 */
routes.get('/:name',
  param('name').notEmpty(),
  validateRequest,
  asyncHandler(characterController.show)
)

export default routes
