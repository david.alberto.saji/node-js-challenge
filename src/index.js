import express from 'express'
import expressJSDocSwagger from 'express-jsdoc-swagger'

import './config/environment'
import { errorHandler } from './middleware'
import routes from './routes'
import './models'

const options = {
  info: {
    title: 'Rick and Morty API',
    description: 'Pinflag node js challenge',
    version: '1.0.0'
  },
  baseDir: __dirname,
  filesPattern: './routes/*.js',
  swaggerUIPath: '/api-docs',
  exposeSwaggerUI: true,
  notRequiredAsNullable: false
}

const app = express()
const port = process.env.PORT || 5000

expressJSDocSwagger(app)(options)

app.use(express.json())
app.use('/', routes)
app.use(errorHandler)

const startServer = () => {
  app.listen(port, () => {
    console.log(`API running on http://127.0.0.1:${port}/`)
  })
}

if (process.env.NODE_ENV !== 'test') {
  startServer()
}

export { app }
