import { validationResult } from 'express-validator'
import BaseController from '../controllers/base'

export default function validateRequest (req, res, next) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return new BaseController().ErrorBadRequest(res, { errors: errors.array() })
  }
  next()
}
