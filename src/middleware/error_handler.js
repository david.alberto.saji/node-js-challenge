import BaseController from '../controllers/base'

export default function errorHandler (err, req, res, next) {
  if (err.response && err.response.status === 404) {
    return new BaseController().NotFound(res, { error: 'There is nothing here' })
  }
  return new BaseController().InternalError(res, err)
}
