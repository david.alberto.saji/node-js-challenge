import { matchedData } from 'express-validator'
import models from '../models'
import BaseController from './base'
import CharacterService from '../services/character_services'

export default class CharacterController extends BaseController {
  CharacterController () { }

  async index (req, res) {
    const { limit } = matchedData(req, { locations: ['query'] })
    const results = await new CharacterService().findMany(limit)
    return super.Success(res, results)
  }

  async create (req, res) {
    const bodyData = matchedData(req, { locations: ['body'] })
    const character = await models.Character.create(bodyData)
    return super.Created(res, character)
  }

  async show (req, res) {
    const { name } = matchedData(req, { locations: ['params'] })
    let character = await models.Character.findOne({ where: { name }, attributes: { exclude: ['id'] } })
    if (!character) {
      character = await new CharacterService().findByName(name)
      if (!character) {
        return super.NotFound(res, { error: 'There is nothing here' })
      }
    }
    return super.Success(res, character)
  }
}
