import request from 'supertest'
import nock from 'nock'

import models, { sequelize } from './models'
import { app } from './index'

const fakeData = [
  {
    info: {
      count: 5,
      pages: 2,
      next: 'https://rickandmortyapi.com/api/character/?page=2',
      prev: null
    },
    results: [
      {
        id: 1,
        name: 'Rick Sanchez',
        status: 'Alive',
        species: 'Human',
        type: '',
        gender: 'Male',
        origin: {
          name: 'Earth (C-137)',
          url: 'https://rickandmortyapi.com/api/location/1'
        },
        location: {
          name: 'Citadel of Ricks',
          url: 'https://rickandmortyapi.com/api/location/3'
        },
        image: 'https://rickandmortyapi.com/api/character/avatar/1.jpeg',
        episode: [
          'https://rickandmortyapi.com/api/episode/1',
          'https://rickandmortyapi.com/api/episode/2'
        ],
        url: 'https://rickandmortyapi.com/api/character/1',
        created: '2017-11-04T18:48:46.250Z'
      },
      {
        id: 2,
        name: 'Morty Smith',
        status: 'Alive',
        species: 'Human',
        type: '',
        gender: 'Male',
        origin: {
          name: 'unknown',
          url: ''
        },
        location: {
          name: 'Citadel of Ricks',
          url: 'https://rickandmortyapi.com/api/location/3'
        },
        image: 'https://rickandmortyapi.com/api/character/avatar/2.jpeg',
        episode: [
          'https://rickandmortyapi.com/api/episode/1',
          'https://rickandmortyapi.com/api/episode/2'
        ],
        url: 'https://rickandmortyapi.com/api/character/2',
        created: '2017-11-04T18:50:21.651Z'
      },
      {
        id: 3,
        name: 'Summer Smith',
        status: 'Alive',
        species: 'Human',
        type: '',
        gender: 'Female',
        origin: {
          name: 'Earth (Replacement Dimension)',
          url: 'https://rickandmortyapi.com/api/location/20'
        },
        location: {
          name: 'Earth (Replacement Dimension)',
          url: 'https://rickandmortyapi.com/api/location/20'
        },
        image: 'https://rickandmortyapi.com/api/character/avatar/3.jpeg',
        episode: [
          'https://rickandmortyapi.com/api/episode/6',
          'https://rickandmortyapi.com/api/episode/7'
        ],
        url: 'https://rickandmortyapi.com/api/character/3',
        created: '2017-11-04T19:09:56.428Z'
      }
    ]
  },
  {
    info: {
      count: 5,
      pages: 2,
      next: null,
      prev: 'https://rickandmortyapi.com/api/character/?page=1'
    },
    results: [
      {
        id: 5,
        name: 'Jerry Smith',
        status: 'Alive',
        species: 'Human',
        type: '',
        gender: 'Male',
        origin: {
          name: 'Earth (Replacement Dimension)',
          url: 'https://rickandmortyapi.com/api/location/20'
        },
        location: {
          name: 'Earth (Replacement Dimension)',
          url: 'https://rickandmortyapi.com/api/location/20'
        },
        image: 'https://rickandmortyapi.com/api/character/avatar/5.jpeg',
        episode: [
          'https://rickandmortyapi.com/api/episode/6',
          'https://rickandmortyapi.com/api/episode/7'
        ],
        url: 'https://rickandmortyapi.com/api/character/5',
        created: '2017-11-04T19:26:56.301Z'
      },
      {
        id: 6,
        name: 'Abadango Cluster Princess',
        status: 'Alive',
        species: 'Alien',
        type: '',
        gender: 'Female',
        origin: {
          name: 'Abadango',
          url: 'https://rickandmortyapi.com/api/location/2'
        },
        location: {
          name: 'Abadango',
          url: 'https://rickandmortyapi.com/api/location/2'
        },
        image: 'https://rickandmortyapi.com/api/character/avatar/6.jpeg',
        episode: [
          'https://rickandmortyapi.com/api/episode/27'
        ],
        url: 'https://rickandmortyapi.com/api/character/6',
        created: '2017-11-04T19:50:28.250Z'
      }
    ]
  }
]

beforeEach(() => models.Character.sync({ force: true }))
afterAll(() => sequelize.close())

describe('get characters from Rick and Morty API', () => {
  beforeEach(() => {
    nock.cleanAll()
  })

  test('get 4 characters', async () => {
    nock('https://rickandmortyapi.com/api/character')
      .get('/')
      .reply(200, fakeData[0])
      .get('/')
      .query({ page: 2 })
      .reply(200, fakeData[1])

    const response = await request(app)
      .get('/character')
      .query({ limit: 4 })
      .expect(200)
      .expect('Content-Type', /json/)

    expect(response.body).toEqual([
      {
        name: 'Rick Sanchez',
        status: 'Alive',
        species: 'Human',
        origin: 'Earth (C-137)'
      },
      {
        name: 'Morty Smith',
        status: 'Alive',
        species: 'Human',
        origin: 'unknown'
      },
      {
        name: 'Summer Smith',
        status: 'Alive',
        species: 'Human',
        origin: 'Earth (Replacement Dimension)'
      },
      {
        name: 'Jerry Smith',
        status: 'Alive',
        species: 'Human',
        origin: 'Earth (Replacement Dimension)'
      }
    ])
  })
})

describe('create character', () => {
  test('create and save character in db', async () => {
    const oldCharacters = await models.Character.findAll()
    expect(oldCharacters).toHaveLength(0)

    await request(app)
      .post('/character')
      .send({
        name: 'Rick Sanchez',
        status: 'Alive',
        species: 'Human',
        origin: 'Earth (C-137)'
      })
      .expect(201)
      .expect('Content-Type', /json/)

    const newCharacters = await models.Character.findAll()
    expect(newCharacters).toHaveLength(1)
    expect(newCharacters[0].toJSON()).toEqual({
      id: 1,
      name: 'Rick Sanchez',
      status: 'Alive',
      species: 'Human',
      origin: 'Earth (C-137)'
    })
  })
})

describe('get character by name', () => {
  beforeEach(() => {
    nock.cleanAll()
  })

  test('character exists in db', async () => {
    await models.Character.create({
      name: 'Test',
      status: 'Alive',
      species: 'Bot',
      origin: 'Testland'
    })

    const response = await request(app)
      .get('/character/Test')
      .expect(200)
      .expect('Content-Type', /json/)

    expect(response.body).toEqual({
      name: 'Test',
      status: 'Alive',
      species: 'Bot',
      origin: 'Testland'
    })
  })

  test('character does not exist in db but exists in fakeData', async () => {
    const characters = await models.Character.findAll()
    expect(characters).toHaveLength(0)

    nock('https://rickandmortyapi.com/api/character')
      .get('/')
      .query({ name: 'Rick Sanchez' })
      .reply(200, fakeData[0])

    const response = await request(app)
      .get('/character/Rick Sanchez')
      .expect(200)
      .expect('Content-Type', /json/)

    expect(response.body).toEqual({
      name: 'Rick Sanchez',
      status: 'Alive',
      species: 'Human',
      origin: 'Earth (C-137)'
    })
  })

  test('character does not exist in db and does not exist in fakeData', async () => {
    const characters = await models.Character.findAll()
    expect(characters).toHaveLength(0)

    nock('https://rickandmortyapi.com/api/character')
      .get('/')
      .query({ name: 'Test' })
      .reply(404, {
        error: 'There is nothing here'
      })

    const response = await request(app)
      .get('/character/Test')
      .expect(404)
      .expect('Content-Type', /json/)

    expect(response.body).toEqual({
      error: 'There is nothing here'
    })
  })
})
