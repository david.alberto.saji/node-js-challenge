export function extractCharacterData (character) {
  const { name, status, species, origin: { name: origin } } = character
  return { name, status, species, origin }
}
